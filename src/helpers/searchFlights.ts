import { Airport, Flight } from '../type';

const getRandomInt = (max: number) => {
  return Math.floor(Math.random() * max);
};
const getRandomLetter = () => {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz';
  return alphabet[Math.floor(Math.random() * alphabet.length)];
};

const searchFlightsDummy = (from: Airport, to: Airport): Promise<Flight[]> => {
  // I have to do this horrible hack to use the free version of mockapi and get
  // some kind of dummy result.
  // I created also the https://64b15152062767bc48260653.mockapi.io/flights API
  // but it happens too often to get empty result
  const url = new URL('https://64b15152062767bc48260653.mockapi.io/airport');
  url.searchParams.append('name', getRandomLetter());
  url.searchParams.append('page', '1');
  url.searchParams.append('limit', String(getRandomInt(10)));
  return fetch(url)
    .then((res) => res.json())
    .then((rawFlights: { name: string }[]) =>
      rawFlights
        .map(({ name }: { name: string }) => {
          const randomStuff = getRandomInt(10);
          const hasStopOver = randomStuff % 2 === 0;

          return {
            name,
            stopover: hasStopOver ? `${name}-stopover` : undefined,
            price: getRandomInt(500).toString(),
          };
        })
        .sort((a, b) => {
          if (a.price > b.price) {
            return 1;
          } else if (a.price < b.price) {
            return -1;
          } else {
            return 0;
          }
        }),
    );
};

const searchFlights = (from: Airport, to: Airport): Promise<Flight[]> => {
  const url = new URL(`http://localhost:8000/api/flights/${from.code}-${to.code}`);

  return fetch(url)
    .then((res) => res.json())
    .then((rawFlights: { price: string; stopover: string | null }[]) =>
      rawFlights.map(({ price, stopover }) => ({
        stopover: stopover ?? undefined,
        price,
      })),
    );
};

export default process.env.NODE_ENV === 'development' ? searchFlights : searchFlightsDummy;
