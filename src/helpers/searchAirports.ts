import { Airport } from '../type';

const searchAirportsDummy = (query: string): Promise<Airport[]> => {
  const url = new URL('https://64b15152062767bc48260653.mockapi.io/airport');
  url.searchParams.append('name', query);
  url.searchParams.append('page', '1');
  url.searchParams.append('limit', '10');
  return fetch(url).then((res) => res.json());
};

const searchAirports = (query: string): Promise<Airport[]> => {
  const url = new URL(`http://localhost:8000/api/airports/search/${query}`);

  return fetch(url).then((res) => res.json());
};

export default process.env.NODE_ENV === 'development' ? searchAirports : searchAirportsDummy;
