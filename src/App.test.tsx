import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import useFlights from './hooks/useFlights';

jest.mock('./hooks/useFlights');

jest.mock('./components/Loading', () => () => {
  return <div>Loading</div>;
});
jest.mock('./components/EmptyState', () => () => {
  return <div>Empty State</div>;
});
jest.mock('./components/NoResults', () => () => {
  return <div>No Results</div>;
});
jest.mock('./components/FlightsResult', () => () => {
  return <div>Flights Result</div>;
});
jest.mock('./components/SearchBox', () => () => {
  return <div>Search Box</div>;
});

const mockedUseFlights = useFlights as jest.MockedFunction<typeof useFlights>;
const defaultUseFlightsMockReturn = {
  isLoading: false,
  onSearch: () => {},
  flights: undefined,
};

describe('App', () => {
  it('should render empty state by default and the search box', () => {
    mockedUseFlights.mockReturnValue(defaultUseFlightsMockReturn);
    render(<App />);
    expect(screen.getByText(/empty state/i)).toBeInTheDocument();
    expect(screen.getByText(/search box/i)).toBeInTheDocument();
  });

  it('should render loading if is the user never gets results and still loading', () => {
    mockedUseFlights.mockReturnValue({ ...defaultUseFlightsMockReturn, isLoading: true });
    render(<App />);
    expect(screen.getByText(/loading/i)).toBeInTheDocument();
  });

  it('should render loading if is the user already gets results and still loading', () => {
    mockedUseFlights.mockReturnValue({
      ...defaultUseFlightsMockReturn,
      isLoading: true,
      flights: [{ price: '2000', stopover: 'Novara' }],
    });
    render(<App />);
    expect(screen.getByText(/loading/i)).toBeInTheDocument();
  });

  it('should render loading if is the user gets empty result and still loading', () => {
    mockedUseFlights.mockReturnValue({
      ...defaultUseFlightsMockReturn,
      isLoading: true,
      flights: [],
    });
    render(<App />);
    expect(screen.getByText(/loading/i)).toBeInTheDocument();
  });

  it('should render no result if the user get an empty set of result', () => {
    mockedUseFlights.mockReturnValue({
      ...defaultUseFlightsMockReturn,
      isLoading: false,
      flights: [],
    });
    render(<App />);
    const linkElement = screen.getByText(/no results/i);
    expect(linkElement).toBeInTheDocument();
  });

  it('should render the flight results if the user get results', () => {
    mockedUseFlights.mockReturnValue({
      ...defaultUseFlightsMockReturn,
      isLoading: false,
      flights: [],
    });
    render(<App />);
    expect(screen.getByText(/no results/i)).toBeInTheDocument();
  });
});
