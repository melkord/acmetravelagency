import React, { useState, useMemo } from 'react';

import { Airport } from './type';

import useFlights from './hooks/useFlights';

import Loading from './components/Loading';
import EmptyState from './components/EmptyState';
import NoResults from './components/NoResults';
import FligtsResult from './components/FlightsResult';
import SearchBox from './components/SearchBox';

type State = 'IS_LOADING' | 'EMPTY_STATE' | 'NO_RESULTS' | 'HAS_RESULTS';

const App = () => {
  const [airportFrom, setAirportFrom] = useState<Airport | undefined>();
  const [airportTo, setAirportTo] = useState<Airport | undefined>();

  const { isLoading, onSearch, flights } = useFlights();

  const state = useMemo<State>(() => {
    if (isLoading) {
      return 'IS_LOADING';
    }
    if (flights === undefined) {
      return 'EMPTY_STATE';
    }
    if (flights.length === 0) {
      return 'NO_RESULTS';
    }

    return 'HAS_RESULTS';
  }, [flights, isLoading]);

  return (
    <div className='container mx-auto px-8'>
      <h1 className='text-3xl text-center'>Welcome in Acme Travel Agency</h1>
      <SearchBox
        airportTo={airportTo}
        airportFrom={airportFrom}
        setAirportFrom={setAirportFrom}
        setAirportTo={setAirportTo}
        onSubmit={() => {
          if (airportFrom && airportTo) {
            onSearch(airportFrom, airportTo);
          }
        }}
      />
      <div className='container mx-auto px-8'>
        {state === 'IS_LOADING' && <Loading />}
        {state === 'EMPTY_STATE' && <EmptyState />}
        {state === 'NO_RESULTS' && <NoResults />}
        {flights && state === 'HAS_RESULTS' && <FligtsResult flights={flights} />}
      </div>
    </div>
  );
};

export default App;
