import React from 'react';
import { Flight } from '../../type';

const FligtsResult = ({ flights }: { flights: Flight[] }) => (
  <table className='min-w-full text-left text-sm font-light'>
    <thead className='border-b font-medium dark:border-neutral-500'>
      <tr>
        <th scope='col' className='px-6 py-4'>
          Price
        </th>
        <th scope='col' className='px-6 py-4'>
          Stopover Airport
        </th>
      </tr>
    </thead>
    <tbody>
      {flights.map(({ price, stopover }) => {
        return (
          <tr className='class="border-b dark:border-neutral-500"'>
            <td className='whitespace-nowrap px-6 py-4'>{price}</td>
            <td className='whitespace-nowrap px-6 py-4'>{stopover ?? '-'}</td>
          </tr>
        );
      })}
    </tbody>
  </table>
);

export default FligtsResult;
