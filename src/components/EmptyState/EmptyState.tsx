import React from 'react';

const EmptyState = () => <img src='/assets/empty_state.png' alt='No search provided' />;

export default EmptyState;
