import React, { useEffect, useState } from 'react';
import { Combobox } from '@headlessui/react';
import { Airport } from '../../type';
import searchAirports from '../../helpers/searchAirports';

type People = { id: number; name: string };

type AirportSelectorProps = {
  selectedAirport?: Airport;
  onAirportSet: (selectedAirport: Airport) => void;
  id: string;
  label: string;
};

const AirportSelector = ({ selectedAirport, onAirportSet, id, label }: AirportSelectorProps) => {
  const [airports, setAirports] = useState<Airport[]>([]);

  const [query, setQuery] = useState<string | undefined>();

  useEffect(() => {
    if (query) {
      searchAirports(query).then(setAirports);
    }
  }, [query]);

  return (
    <div className='w-min'>
      <label htmlFor={id} className='block'>
        {label}
      </label>
      <Combobox value={selectedAirport} onChange={onAirportSet}>
        <Combobox.Input
          className='border-solid border-2 border-sky-500 w-64 bg-white'
          onChange={(event) => setQuery(event.target.value)}
          displayValue={(person: People) => person.name}
          id={id}
          autoComplete='off'
        />
        {airports.length > 0 && (
          <Combobox.Options className='border-solid border-2 border-sky-500 w-64 fixed bg-white bg-solid z-10'>
            {airports.map((airports) => (
              <Combobox.Option key={airports.id} value={airports} className='cursor-pointer'>
                {airports.name}
              </Combobox.Option>
            ))}
          </Combobox.Options>
        )}
      </Combobox>
    </div>
  );
};
export default AirportSelector;
