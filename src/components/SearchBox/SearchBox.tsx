import React, { useMemo } from 'react';

import AirportSelector from '../AirportSelector';
import { Airport } from '../../type';
type SearchBoxProps = {
  airportFrom?: Airport;
  setAirportFrom: (airportFrom: Airport) => void;
  airportTo?: Airport;
  setAirportTo: (airportTo: Airport) => void;
  onSubmit: () => void;
};
const SearchBox = ({
  airportFrom,
  setAirportFrom,
  airportTo,
  setAirportTo,
  onSubmit,
}: SearchBoxProps) => {
  const submitIsDisabled = useMemo(() => !airportFrom || !airportTo, [airportFrom, airportTo]);

  return (
    <div className='bg-zinc-50'>
      <div className='grid grid-cols-2'>
        <AirportSelector
          selectedAirport={airportFrom}
          onAirportSet={setAirportFrom}
          id='airportFrom'
          label='Choose departure airport'
        />
        <AirportSelector
          selectedAirport={airportTo}
          onAirportSet={setAirportTo}
          id='airportTo'
          label='Choose arrival airport'
        />
      </div>
      <button
        className='my-4 px-4 border border-sky-500 rounded disabled:opacity-25'
        disabled={submitIsDisabled}
        type='submit'
        onClick={(e) => {
          e.preventDefault();
          onSubmit();
        }}
      >
        Search
      </button>
    </div>
  );
};

export default SearchBox;
