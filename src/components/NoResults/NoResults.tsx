import React from 'react';

const NoResults = () => <img src='/assets/flight_not_found.png' alt='No flight found' />;

export default NoResults;
