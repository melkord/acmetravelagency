import { useState } from 'react';
import { Airport, Flight } from '../type';
import searchFlights from '../helpers/searchFlights';

const useFlights = (): {
  isLoading: boolean;
  flights?: Flight[];
  onSearch: (airportFrom: Airport, airportTo: Airport) => void;
} => {
  const [isLoading, setIsLoading] = useState(false);
  const [flights, setFlights] = useState<Flight[] | undefined>();
  const onSearch = (airportFrom: Airport, airportTo: Airport) => {
    setIsLoading(true);
    searchFlights(airportFrom, airportTo)
      .then(setFlights)
      .catch(() => {
        console.error('ERRORE in useFlights');
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return { isLoading, onSearch, flights };
};

export default useFlights;
