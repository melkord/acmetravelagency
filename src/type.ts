export type Airport = {
  id: number;
  name: string;
  code: string;
};

export type Flight = {
  price: string;
  stopover?: string;
};
