# Acme Travel Agency

- [Acme Travel Agency](#acme-travel-agency)
  - [Project Description](#project-description)
  - [Getting Started](#getting-started)
    - [Available Scripts](#available-scripts)
      - [`yarn start`](#yarn-start)
      - [`yarn test`](#yarn-test)
      - [`yarn build`](#yarn-build)
      - [`yarn eject`](#yarn-eject)
  - [ToDo](#todo)

## Project Description

Write a React app that displays the most convenient flights between two airports.
The backend must be a PHP algorithm that finds the lowest price, assuming at most 1 stopover. Data must come from a database.

[Online demo](https://acme-travel-agency.netlify.app/)

## Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Available Scripts

In the project directory, you can run:

#### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

🚨🚨Note that you need to run the server in local env. You can find the server following [this repo link](https://gitlab.com/melkord/acmetravelagencyserver)

#### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

#### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

#### `yarn eject`

## ToDo

- [x] Create MVP
- [x] Add tests
- [x] Wire the real backend API when ready
- [ ] Remove from airport the one already selected
- [ ] Manage errors with an exaustive message
- [ ] Add debounce when an airport is searched
